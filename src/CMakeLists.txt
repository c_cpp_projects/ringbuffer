add_executable(${EXECUTABLE_NAME}
  main.c
)

include_directories(
  ${CMAKE_HOME_DIRECTORY}/src
  ${CMAKE_HOME_DIRECTORY}/src/ringbuffer
)

target_link_libraries(${EXECUTABLE_NAME}
  ringbuffer
)

install(
  TARGETS ${EXECUTABLE_NAME}
  RUNTIME
  DESTINATION ${CMAKE_HOME_DIRECTORY}/bin
)