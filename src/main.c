#include <stdio.h>
#include <stdlib.h>

#include "ringbuffer/ringbuffer.h"

size_t test1Write(size_t loops);
size_t test2ReadWrite(void);

int main() {
  // printf("test1Write\nErrors: %lu\n\n", test1Write(1));
  printf("test2ReadWrite\nErrors: %lu\n\n", test2ReadWrite());

  // char* arr = (char*) calloc(20, sizeof(char));
  // size_t i;
  // size_t size = 24;
  // for (i = 0; i < size; i++) arr[i] = i;
  // for (i = 0; i < size; i++) printf("[%lu]: %c\n", i, arr[i]);

  return 0;
}

size_t test1Write(size_t loops) {
  TRingBuffer buffer = RingBufferCreate(256);
  size_t errorCount = 0;

  for (size_t i = 0; i < loops; i++) {
    for (unsigned int j = 0; j < 256; j++) {
      if (!RingBufferWrite(buffer, j)) errorCount++;
    }
  }

  RingBufferDestroy(buffer);

  return errorCount;
}

size_t test2ReadWrite(void) {
  size_t size = 10;
  TRingBuffer buffer = RingBufferCreate(size);
  size_t errorCount = 0;
  unsigned char data;

  size_t i;
  for (i = 0; i < size; i++) {
    if (!RingBufferWrite(buffer, (unsigned char) i)) errorCount++;
  }

  for (i = 0; i < size + 5; i++) {
    if (!RingBufferRead(buffer, &data)) errorCount++;
    printf("[%lu]: %c\t(ascii %d)\n", i, data, data);
    data = 'A';
  }

  RingBufferDestroy(buffer);

  return errorCount;
}
