#include "ringbuffer.h"

/******************** Declaration of Private Structs ********************/

struct RingBufferStruct {
  size_t wIndex;
  size_t rIndex;

  // The underlaying buffer.
  unsigned char* buffer;

  size_t size;
};

/******************** Definition of Public Functions ********************/

TRingBuffer RingBufferCreate(size_t size) {
  TRingBuffer ringBuffer = (TRingBuffer) calloc(1, sizeof(struct RingBufferStruct));

  if (NULL == ringBuffer) return NULL;

  ringBuffer->size = size;
  ringBuffer->buffer = (unsigned char*) malloc(size * sizeof(unsigned char));

  if (NULL == ringBuffer->buffer) {
    RingBufferDestroy(ringBuffer);
    return NULL;
  }

  return ringBuffer;
}

void RingBufferDestroy(TRingBuffer ringBuffer) {
  if (NULL == ringBuffer) return;
  if (NULL != ringBuffer->buffer) free(ringBuffer->buffer);
  free(ringBuffer);
}

bool RingBufferRead(TRingBuffer ringBuffer, unsigned char* byte) {
  if (NULL == ringBuffer) return false;

  // buffer is empty
  if (ringBuffer->rIndex == ringBuffer->wIndex) return false;

  size_t r = ringBuffer->rIndex;
  r++;

  // handle index overflow
  if (r >= ringBuffer->size) r = 0;

  *byte = ringBuffer->buffer[ringBuffer->rIndex];
  ringBuffer->rIndex = r;

  return true;
}

bool RingBufferIsEmpty(const TRingBuffer ringBuffer) {
  if (NULL == ringBuffer) return true;
  if (ringBuffer->rIndex == ringBuffer->wIndex) return true;
  return false;
}

bool RingBufferIsFull(const TRingBuffer ringBuffer) {
  if (NULL == ringBuffer) return false;

  size_t w = ringBuffer->wIndex;
  w++;
  // handle index overflow
  if (w >= ringBuffer->size) w = 0;

  if (w == ringBuffer->rIndex) return true;
  return false;
}

bool RingBufferWrite(TRingBuffer ringBuffer, unsigned char byte) {
  if (NULL == ringBuffer) return false;

  size_t w = ringBuffer->wIndex;
  w++;

  // handle index overflow
  if (w >= ringBuffer->size) w = 0;

  // buffer is full
  if (w == ringBuffer->rIndex) return false;

  ringBuffer->buffer[ringBuffer->wIndex] = byte;
  ringBuffer->wIndex = w;

  return true;
}
