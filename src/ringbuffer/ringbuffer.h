#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include <stdbool.h>
#include <stdlib.h>

typedef struct RingBufferStruct* TRingBuffer;

/**
 * @brief Create a new RingBuffer.
 *
 * @param size The size of this RingBuffer.
 *
 * @return Pointer to a new RingBuffer.
 */
TRingBuffer RingBufferCreate(size_t size);

/**
 * @brief Destroy this RingBuffer, it should not be used for
 *        any funcntions after it has been destroyed.
 */
void RingBufferDestroy(TRingBuffer ringBuffer);

/**
 * @brief Read data from this RingBuffer.
 *
 * @param ringBuffer The RingBuffer from which you want to read data.
 * @param byte       The address of the variable where you want to store
 *                   the read byte.
 *
 * @return True if there was data to be read, false if this RingBuffer
 *         was empty.
 */
bool RingBufferRead(TRingBuffer ringBuffer, unsigned char* byte);

bool RingBufferIsEmpty(const TRingBuffer ringBuffer);

bool RingBufferIsFull(const TRingBuffer ringBuffer);

/**
 * @brief Write data to this RingBuffer.
 *
 * @param ringBuffer The RingBuffer to which you want to write data to.
 * @param byte       The byte you want to write to this RingBuffer.
 *
 * @return True if the data was successfully written to this RingBuffer,
 *         false if this RingBuffer was full.
 */
bool RingBufferWrite(TRingBuffer ringBuffer, unsigned char byte);

#endif  // RINGBUFFER_H